# Triple Boot
- Ubuntu 18.04.4 LTS:    to work
- Windows 10 Pro 64bits: to play
- MacOS Catalina 1.15.7: to be

# Macbook Pro 15
- Mid-2012
- i7 2.6GHz
- RAM 16GB 1600MHz 9-9-9-28
- SSD Samsung 870 
EVO 1TB SATA 2,5"

# Dimensions et poids
- Hauteur:    2,41 cm
- Largeur:    36,4 cm
- Profondeur: 24,9 cm
- Poids:      2,56 k
g

# Install Last MacOS (Catalina) on New SDD or Erase HDD/SDD for new MacOS installation
At start up:
- Option(Alt)+Cmd+R: Internet Recovery to install last macOS version supported by device

Utilitaire de disques  
Présentation: Afficher tous les appareils  
Select: Samsung SSD 870 EVO 1TB Media | Samsung SSD 840 PRO Series Media  
Effacer:  
--- macOS  
--- APFS  
--- Table de partition GUID  
--- Effacer  
--- OK  
Utilitaire de disques: Quitter Utilitaire de disque

# New MacOS installation (Catalina)
(At start up: Option(Alt)+Cmd+R)  

Réinstaller macOS  
--- macOS Catalina: Continuer / Accepter / Accepter 
--- Select: "macOS" + Installer  
- Bienvenue: France + Continuer  
- Langues parlées et écrites: Continuer  
- Données et confidentialtés: Continuer  
- Transférer des données vers ce Mac: Ne pas transférer des données maintenant + Continuer  
- Connectez-vous avec votre identifiant Apple: Configurer plus tard + Ignorer  
- Conditions générales: Accepter + Accepter  
- Créer un compte d'ordinateur: remplir + Continuer  
- Configuration express: ...  
- ...  

# New MacOS partitions (Catalina)
(At start up: Option(Alt)+Cmd+R)  

- Utilitaire de disques  
- Présentation: Afficher tous les appareils  
- Select: Samsung SSD 870 EVO 1TB Media | Samsung SSD 840 PRO Series Media  
- Partitionner  
--- Partitionner  
--- '+' 2 fois  
--- Sizes: MS-DOS (FAT) & ExFAT Volumes in MAJ !!!  
macOS		102912	APFS	(2 x 51456)  
SWAP		  8274	ExFAT  
UBUNTU		 16640	ExFAT  
--- Sizes: MS-DOS (FAT) & ExFAT Volumes in MAJ !!!  
macOS		644818	APFS  
SWAP		 32768	ExFAT  
UBUNTU		322409	ExFAT  
--- Appliquer + Partitionner + OK  
--- Effacer SWAP & UBUNTU pour vérifier Nom et Format  
--- Effacer SWAP & UBUNTU pour changer le Format:APFS  

# Obtenir Windows10 pour Boot Camp
Start Catalina  
Download Windows 10: https://www.microsoft.com/fr-fr/software-download/windows10  
--- Sélectionner une édition: Windows 10  
--- Sélectionner une langue de produit: Français  
--- Windows 10 Français: 64-bit Télécharger  

# Installer Windows 10 avec Boot Camp
Ouvrir Assistant Boot Camp  
--- Continuer  
--- Selectionner la dernière case si vous avez déjà une clé USB (Windows10+drivers), sinon cocher les 3 cases + Continuer  
--- Image ISO:  
--- Disque de destination:  
--- Continuer + Continuer  
--- Créer une partition pour Windows: Diviser en parts égales  
--- Password(s)  
  
Redémarrage: installer Windows 10  
--- Suivant, Suivant, Suivant  
--- Formatter et Renommer la Partition en Windows  
--- Choisir la partition BOOTCAMP / Windows  
--- Finaliser Installation Windows 10  
--- Finaliser Installation driver Boot Camp pour Windows  
--- Terminer + Redémarrer Système  
--- Renommer la Partition en Windows au besoin  

Activer Windows 10 Professional OEM  
Il existe des clés pas chères (www.kinguin.net)

# Clé USB Ubuntu 18.04
Formater Disk FAT32  
-> rufus 3.15  
ubuntu-18.04.4-desktop-amd64.iso  
Installation standard de Windows  
--- MBR  
--- BIOS ou UEFI  
--- FAT32  
--- 8192  

# boot on Ubuntu USB Key
Start up with "Option(alt)"  
EFI Boot  
Try Ubuntu Ubuntu without installing  
open terminal Ctrl+Alt+T  
Resize with disk  
ubiquity -b (minus is ")°" key)  
English / French / French-French (Macintosh)  
minimal + uncheck Downloads updates + check Install third party  
Something else  
Change... : swap area + OK  
Change... : Ext4 journaling file system + Mount point : "/" + OK  

My partitions are formated as : EFI, macOS, Windows, Swap, Ubuntu  
	/dev/sda
	free space		0 MB
	/dev/sda1 efi	209 MB			EFI
	/dev/sda2		321999 MB		macOS
	free space		0 MB
	/dev/sda3 ntfs	322817 MB		Windows
	/dev/sda4 swap	32768 MB		make swap area
	/dev/sda5 ext4	322408 MB		make Ext4 journaling file system
	free space		0 MB

Select "/dev/sda5 ext4" + Install Now + Continue + Continue  
Name ... + Continue  
Continue Testing  
Power Off  
Retirer Clé USB  

# ReFind
Copy on FAT32 USB Key (here named "TOOLS")  
Option(Alt)+Cmd+R  
Utilitaires Terminal  
ls /Volumes/  
cd /Volumes/TOOLS/  
ls  
./refind-install  
reboot  
)° to hide EFI  

# Install Minimalistic rEFInd theme
source: https://github.com/evanpurkhiser/rEFInd-minimal  
source: https://www.youtube.com/watch?v=91Bb6l9H1uo  
Nte: Configurez ERFInd avant (icones affichées ...)  

Open Terminal in MacOS :  
sudo mkdir /Volumes/EFI  
sudo mount -t msdos /dev/disk0s1 /Volumes/EFI  
Quitter Terminal  

Aller / Ordinateur  
Open EFI  
Go into /EFI/refind/  
Nouveau dossier: "themes"  
Unzip "rEFInd-minimal" into "themes":  
-> /EFI/refind/themes/rEFInd-minimal/  

Open Terminal in MacOS :  
cd /Volumes/EFI/EFI/refind  
echo "include themes/rEFInd-minimal/theme.conf" > refind.conf  
Quitter Terminal  

Ouvrir "/EFI/refind/themes/refing.conf" avec "TextEdit"  
A la fin du fichier, ajouter la ligne suivante:  

# Changer l'ordre présentation refind ...  
  
# Vérifications et Configurations
macOS  
Utilitaire / Informations système / SATA/SATA Express: Prise en charge de TRIM = Oui  
Mise à jour logiciel  
Préférences système:  
--- Bureau et économiseur d'écran  
Economiseur d'énergie  
Batterie: Afficher le pourcentage  
!!! rEFInd shall be re-install after MacOS updates !!!  

Windows  
Invite de commandes (admin)  
fsutil behavior query DisableDeleteNotify  
> NTFS DisableDeleteNotify = 0  (Désactivé)  
> ReFS DisableDeleteNotify = 0  (Désactivé)  
0  (Désactivé) = TRIM activé: OK  
if return 1 (disable) -> "fsutil behavior set DisableDeleteNotify 0"  
  
# Ubuntu CPU Running Hot?  
source:  
https://linuxnewbieguide.org/how-to-install-linux-on-a-macintosh-computer/#Step_3_Installing_Linux_on_that_Mac  
  
$sudo -s
grep . -r /sys/firmware/acpi/interrupts/

You’ll see a list of probably 70 or so lines relating to the firmware that works with ACPI (Advanced Configuration and Power Interface).
Most of these are doing their thing quite happily, but you’ll find one (or maybe even two) of them that has a number like gpe16 has a large number beside it.
It’ll look like this:
/sys/firmware/acpi/interrupts/gpe16:  225420     STS enabled      unmasked

When you think you’ve found it, you can simply disable it, but first, just back up the file, just in case you make the wrong change.
Note I am using gpe16 as that’s the one I found the problem with, yours is probably different:
cp /sys/firmware/acpi/interrupts/gpe16 /root/gpe16.backup
echo "disable" > /sys/firmware/acpi/interrupts/gpe16

If after a few seconds (say 30-60), the CPU fans stop whirring, and system monitor/top starts showing normal usage statistics, then you know it’s the right one.
If it isn’t the right one simply echo “enable”, rather than disable.

To make the change permanent, do the following tasks, again at the terminal, changing the value ’16’ to the value you used:
crontab -e

Add the below line to the crontab, so it will be executed every startup/reboot:
@reboot echo "disable" > /sys/firmware/acpi/interrupts/gpe16

Save/exit. Then, to make it work also after wakeup from suspend:
touch /etc/pm/sleep.d/30_disable_gpe16
chmod +x /etc/pm/sleep.d/30_disable_gpe16
vim /etc/pm/sleep.d/30_disable_gpe16

Add this stuff:
```
#!/bin/bash
case "$1" in
    thaw|resume)
        echo disable > /sys/firmware/acpi/interrupts/gpe16 2>/dev/null
        ;;
    *)
        ;;
esac
exit $?
```
  
```
ubuntu-drivers devices
sudo apt-get install lm-sensors
watch sensors
sudo apt install macfanctld
watch sensors
```
  
# Bonus1: Download macOS Catalina:
https://support.apple.com/fr-fr/HT201372  
-> Télécharger macOS  
-> Téléchargez: macOS Big Sur, macOS Catalina, macOS Mojave ou macOS High Sierra  
-> macOS Catalina -> https://apps.apple.com/fr/app/macos-catalina/id1466841314  
-> Get/Obtenir  
Installer macOS Catalina: cmd+Q  
Mise à jour de logiciels: cmd+Q  
App Store: cmd+Q  
Safari: cmd+Q  
Connect USB key  
Aller / Utilitaires / Terminal:  
sudo /Applications/Install\ macOS\ Catalina.app/Contents/Resources/createinstallmedia --volume /Volumes/MyVolume/  
i.e.  
```
sudo /Applications/Install\ macOS\ Catalina.app/Contents/Resources/createinstallmedia --volume /Volumes/16GO/
```
Password:  
Y + Return  
OK  

# Bonus2: Créer une clé USB de boot Windows10 21H1
-> rufus 3.15  
Windows10_21H2_64.iso  
Installation standard de Windows  
GPT  
UEFI (non CSM)  
FAT32  
8192  
démarrer Macbook Pro: maintenir alt (option) to boot on "EFI Boot"  
